# GFU (Gray File Utils)

GFU is a collection of file utility programs and scripts that aim to simplify common file operations. The project is open source and free to use.

## Getting Started
To use GFU, simply clone the repository to your local machine:

```bash
git clone https://gitlab.com/<username>/gfu.git
```

## Programs and Scripts

### ren_files.py

The `ren_files.py` script is a Python script that renames all the files in a directory with a given name and a numbered sequence. To use the script, simply run it from the command line with the following options:

```bash
python3 ren_files.py -d <directory> -n <new_name>
```

- `-d` or `--directory`: Directory containing the files to be renamed.
- `-n` or `--new-name`: New name to use for the renamed files.

For example, to rename all the files in the directory `/path/to/files` with the new name `file` and a numbered sequence starting from 1, you would run the following command:

```
python3 ren_files.py -d /path/to/files -n file
```

## Contributing
If you have any suggestions or improvements for GFU, please feel free to create an issue or submit a pull request.

## License
GFU is licensed under the MIT License. See LICENSE for more information.
