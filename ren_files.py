#!/usr/bin/env python3

#****************************************************************************
#          Name: ren_files.py
#       Version: 1.0
#        Author: Richard Gray <richard@rgray.info>
#   
#   Description: 
#   Part of the GFU file utiles projecct.  This script is for renaming a 
#   number of files at once.  Used for organizing picture or other files.
#****************************************************************************

import os
import argparse

# Define command-line arguments
parser = argparse.ArgumentParser(description='Rename files in a directory with a given name.')
parser.add_argument('-d', '--directory', type=str, required=True, help='Directory containing the files to be renamed.')
parser.add_argument('-n', '--new-name', type=str, required=True, help='New name to use for the renamed files.')

# Parse command-line arguments
args = parser.parse_args()

# Initialize counter for numbering files
i = 1

# Loop through files in directory
for file in os.listdir(args.directory):
    if os.path.isfile(os.path.join(args.directory, file)):
        # Get file extension
        ext = os.path.splitext(file)[1]
        # Rename file with new name and numbered sequence
        os.rename(os.path.join(args.directory, file), os.path.join(args.directory, f"{args.new_name}{i}{ext}"))
        # Increment counter
        i += 1

print(f"All files in {args.directory} renamed with {args.new_name} and numbered sequence.")
